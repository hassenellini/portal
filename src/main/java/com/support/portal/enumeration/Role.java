package com.support.portal.enumeration;

import lombok.Getter;

import static com.support.portal.constant.Authority.ADMIN_AUTHORITIES;
import static com.support.portal.constant.Authority.HR_AUTHORITIES;
import static com.support.portal.constant.Authority.MANAGER_AUTHORITIES;
import static com.support.portal.constant.Authority.SUPER_ADMIN_AUTHORITIES;
import static com.support.portal.constant.Authority.USER_AUTHORITIES;

@Getter
public enum Role {
    ROLE_USER(USER_AUTHORITIES),
    ROLE_HR(HR_AUTHORITIES),
    ROLE_MANAGER(MANAGER_AUTHORITIES),
    ROLE_ADMIN(ADMIN_AUTHORITIES),
    ROLE_SUPER_USER(SUPER_ADMIN_AUTHORITIES);

    private String[] authorities;

    Role(String... authorities) {
        this.authorities = authorities;
    }

}
