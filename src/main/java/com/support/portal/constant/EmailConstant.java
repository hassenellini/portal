package com.support.portal.constant;

public class EmailConstant {
    public static final String SIMPLE_MAIL_TRANSFER_PROTOCOL = "smtp";
    public static final String USERNAME = "hassenellini2@gmail.com";
    public static final String PASSWORD = "**********";
    public static final String FROM_EMAIL = "support-team@proxym.com";
    public static final String CC_EMAIL = "";
    public static final String EMAIL_SUBJECT = "Proxym-it, LLc - New Password";
    public static final String GMAIL_SMTP_SERVER = "smtp.gmail.com";
    public static final String SMTP_HOST = "mail.smtp.host";
    public static final String SMTP_AUTH = "mail.smtp.auth";
    public static final String SMTP_PORT = "mail.smtp.port";
    public static final int DEFAULT_PORT = 587;
    public static final String SMTP_STARTTLS_ENABLE = "mail.smtp.starttls.enable";
    public static final String SMTP_STARTTLS_REQUIRED = "mail.smtp.starttls.required";
    public static final int SOCKET_FACTORY_PORT = 25;
    public static final String SPRING_SMTP_SOCKET_FACTORY_PORT = "spring.mail.properties.mail.smtp.socketFactory.port";
}
