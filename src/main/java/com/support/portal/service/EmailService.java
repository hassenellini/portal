package com.support.portal.service;

import com.sun.mail.smtp.SMTPTransport;
import org.springframework.stereotype.Service;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Date;
import java.util.Properties;

import static com.support.portal.constant.EmailConstant.CC_EMAIL;
import static com.support.portal.constant.EmailConstant.DEFAULT_PORT;
import static com.support.portal.constant.EmailConstant.EMAIL_SUBJECT;
import static com.support.portal.constant.EmailConstant.FROM_EMAIL;
import static com.support.portal.constant.EmailConstant.GMAIL_SMTP_SERVER;
import static com.support.portal.constant.EmailConstant.PASSWORD;
import static com.support.portal.constant.EmailConstant.SIMPLE_MAIL_TRANSFER_PROTOCOL;
import static com.support.portal.constant.EmailConstant.SMTP_AUTH;
import static com.support.portal.constant.EmailConstant.SMTP_HOST;
import static com.support.portal.constant.EmailConstant.SMTP_PORT;
import static com.support.portal.constant.EmailConstant.SMTP_STARTTLS_ENABLE;
import static com.support.portal.constant.EmailConstant.SMTP_STARTTLS_REQUIRED;
import static com.support.portal.constant.EmailConstant.SOCKET_FACTORY_PORT;
import static com.support.portal.constant.EmailConstant.SPRING_SMTP_SOCKET_FACTORY_PORT;
import static com.support.portal.constant.EmailConstant.USERNAME;
import static javax.mail.Message.RecipientType.CC;
import static javax.mail.Message.RecipientType.TO;

@Service
public class EmailService {

    public void sendNewPasswordEmail(String firstName, String password, String email) throws MessagingException {
        Message message = createEmail(firstName, password, email);
        SMTPTransport smtpTransport = (SMTPTransport) getEmailSession().getTransport(SIMPLE_MAIL_TRANSFER_PROTOCOL);
        smtpTransport.connect(GMAIL_SMTP_SERVER, DEFAULT_PORT, USERNAME, PASSWORD);
        smtpTransport.sendMessage(message, message.getAllRecipients());
        smtpTransport.close();
    }

    private Message createEmail(String firstName, String password, String email) throws MessagingException {
        Message message = new MimeMessage(getEmailSession());
        message.setFrom(new InternetAddress(FROM_EMAIL));
        message.setRecipients(TO, InternetAddress.parse(email, false));
        message.setRecipients(CC, InternetAddress.parse(CC_EMAIL, false));
        message.setSubject(EMAIL_SUBJECT);
        message.setText("Hello " + firstName + ", \n \n Your new account password is: " + password + "\n \n The Support Team");
        message.setSentDate(new Date());
        message.saveChanges();
        return message;
    }

    private Session getEmailSession() {
        Properties properties = System.getProperties();
        properties.put(SMTP_HOST, GMAIL_SMTP_SERVER);
        properties.put(SMTP_AUTH, true);
        properties.put(SMTP_PORT, DEFAULT_PORT);
        properties.put(SMTP_STARTTLS_ENABLE, true);
        properties.put(SMTP_STARTTLS_REQUIRED, true);
        properties.put(SPRING_SMTP_SOCKET_FACTORY_PORT, SOCKET_FACTORY_PORT);
        return Session.getInstance(properties, null);
    }
}